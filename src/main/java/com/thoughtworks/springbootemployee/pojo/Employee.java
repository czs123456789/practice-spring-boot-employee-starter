package com.thoughtworks.springbootemployee.pojo;

import lombok.Data;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/18/2023
 **/
@Data
public class Employee {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Integer salary;
    private Long companyId;
}
