package com.thoughtworks.springbootemployee.pojo;

import lombok.Data;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/18/2023
 **/
@Data
public class Company {

    private Long id;
    private String name;

}
