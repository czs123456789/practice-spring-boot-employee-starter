package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.pojo.Company;
import com.thoughtworks.springbootemployee.pojo.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/18/2023
 **/
@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping
    public List<Company> getCompanyList() {
        return companyRepository.getCompanyList();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companyRepository.getCompanyById(id);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable Long id, @RequestBody Company company) {
        return companyRepository.updateCompany(id, company);
    }

    @PostMapping
    public Company save(@RequestBody Company company) {
        return companyRepository.save(company);
    }

    @DeleteMapping("/{id}")
    public void deleteCompanyById(@PathVariable Long id) {
        companyRepository.deleteCompanyById(id);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeeListByCompanyId(@PathVariable Long companyId) {
        return employeeRepository.getEmployeeListByCompanyId(companyId);
    }

    @GetMapping(params = {"page","size"})
    public List<Company> getCompanyPage(@RequestParam Long page, @RequestParam Long size) {
        return companyRepository.getCompanyPage(page,size);
    }

}
