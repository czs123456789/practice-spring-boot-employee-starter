package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.pojo.Company;
import com.thoughtworks.springbootemployee.pojo.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/18/2023
 **/

@Repository
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();

    public Employee save(Employee employee) {
        employee.setId(generateId());
        this.employees.add(employee);
        return employee;
    }

    public List<Employee> getEmployeeList() {
        return employees;
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream().filter(employee -> employee.getId().equals(id)).findFirst().orElse(null);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employees.stream().filter(employee -> employee.getGender().equals(gender)).collect(Collectors.toList());
    }

    public Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId()+1).orElse(1L);
    }

    public Employee update(Long id, Employee employee) {
        Employee newEmployee = getEmployeeById(id);
        if (null == newEmployee) {
            return null;
        }
        newEmployee.setAge(employee.getAge());
        newEmployee.setSalary(employee.getSalary());
        newEmployee.setCompanyId(employee.getCompanyId());
        return newEmployee;
    }

    public void delete(Long id) {
        Employee employee = getEmployeeById(id);
        employees.remove(employee);
    }

    public List<Employee> getPage(Integer page, Integer size) {
        return employees.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    public List<Employee> getEmployeeListByCompanyId(Long companyId) {
        return employees.stream().filter(employee -> employee.getId().equals(companyId)).collect(Collectors.toList());
    }
}
