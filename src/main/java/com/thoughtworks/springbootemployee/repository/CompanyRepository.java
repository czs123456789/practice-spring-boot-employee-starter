package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.pojo.Company;
import com.thoughtworks.springbootemployee.pojo.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/18/2023
 **/
@Repository
public class CompanyRepository {

    private List<Company> companies = new ArrayList<>();

    public List<Company> getCompanyList() {
        return companies;
    }

    public Company getCompanyById(Long id) {
        return companies.stream().filter(company -> company.getId().equals(id)).findFirst().orElse(null);
    }

    public Company updateCompany(Long id, Company company) {
        Company newCompany = getCompanyById(id);
        newCompany.setName(company.getName());
        return newCompany;
    }

    public Long generateId() {
        return companies.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(company -> company.getId()+1).orElse(1L);
    }

    public Company save(Company company) {
        company.setId(generateId());
        this.companies.add(company);
        return company;
    }

    public void deleteCompanyById(Long id) {
        Company company = getCompanyById(id);
        companies.remove(company);
    }

    public List<Company> getCompanyPage(Long page, Long size) {
        return companies.stream().skip( (page - 1) * size).limit(size).collect(Collectors.toList());
    }
}
